# 2023-2024 2. YK+DK toplantısı notları

## Katılım:

### YK üyeleri:
- [x] Serkan Ergüven
- [x] Deniz Pembeçicek
- [x] Lidya Çamlıca
- [x] Eren Açıkyıldız
- [x] Berkay Taşay
- [x] Sinemis Çakar
- [x] Ceren Derya

### DK üyeleri:

- [X] İsmail Aykut Sapaz
- [ ] Bulut Güner
- [x] Cem Sarpkaya

### Aktif üyeler:

- Semih Korkmaz
- İlya Küçükerciyes


## Gündem maddeleri:

- Kulüp odalarının yurt odalarına dönüştürülmesi kararı hakkında yapılacaklar
- Oryantasyon bütçesinin belirlenmesi
- Guitar hero baterisinin tamiri

## İçerik:

- Bayram süresince kulüp odasının boş bırakılmaması kararı alındı, etkinlikler planlanacak.
- BUCON'da neler yapılabileceği konuşuldu.
- Kulüp hoodie'sine ek olarak tişört bastırılması fikri üzerine konuşuldu.
- Oryantasyon icin sponsor aranması fikri üzerine konuşuldu.

## Planlananlar:

- Kulüp web sitesi şifresi istenecek.
- Bateri tamir fiyatı öğrenilecek.
- Oryantasyon ve BUCON etkinlikleri detayları sonraki toplantılarda konuşulacak.
